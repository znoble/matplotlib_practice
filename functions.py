
import sys
import numpy as np
import matplotlib.pyplot as plt

#
# plot activation functions used in neural networks
#
# these functions map the real line to [0,1]
#


if __name__ == "__main__":

    tmp = list(range(-400, 401))
    x = np.array(tmp) / 100

    plt.figure(figsize=(16,9), dpi=80)
    plt.style.use("fivethirtyeight")

    y1 = 1 / (1 + np.exp(-x))  #around 800*4 operations here
    plt.plot(x, y1, label="sigmoid") # also called the logistic function

    y2 = (1 + np.arctan(x)  / (np.pi/2)) / 2
    plt.plot(x, y2, label="arctan")

    y3 = (np.tanh(x) + 1) / 2
    plt.plot(x, y3, label="tanh")

    y4 = x * (x > 0)
    plt.plot(x, y4, label="relu")

    plt.legend()

    plt.savefig("/mnt/d/CS/saved_plots/functions.png")
    
