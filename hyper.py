
import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

if __name__ == "__main__":

# z = x**2 - y**2

    xlist = np.arange(-2.0, 2.0, 0.02)
    ylist = np.arange(-2.0, 2.0, 0.02)
    xlist, ylist = np.meshgrid(xlist, ylist)   #returns all combinations of x,y

    zlist = xlist**2 - ylist**2 #hyperboloid function

    fig = plt.figure(figsize=(12,9), dpi=100)
    fig.gca(projection='3d').plot_surface(xlist,ylist,zlist,cmap='Greys')

    plt.tight_layout()
    plt.show()
    
        
