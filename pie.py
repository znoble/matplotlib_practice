#bar plot in python

import sys
import os
import matplotlib.pyplot as plt


def readages(fname):

    ages = [0] * 9

    with open(fname, "r") as fd:
        for line in fd:
            x = int(line) // 10
            if x > 8:
                x = 8
            ages[x] += 1
            
    return ages

    

if __name__ == "__main__":
    
    age = readages("ages.txt")

    clist = ['red', 'blue','green','brown','purple', 'orange', 'grey']
    labs = ['Teens', '20s', '30s', '40s', '50s', '60s', '70 and up']
    
    plt.figure(figsize=(16,9), dpi=100)
    plt.style.use("fivethirtyeight")
    
    plt.pie(age[1:-1], colors=clist, labels=labs, wedgeprops={"edgecolor":"black"})

    plt.title("Age Groups of Coffee Drinkers")

    plt.savefig("/mnt/d/CS/saved_plots/pie.png")
