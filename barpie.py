#bar plot in python

import sys
import os
import matplotlib.pyplot as plt


def readages(fname):

    ages = [0] * 9

    with open(fname, "r") as fd:
        for line in fd:
            x = int(line) // 10
            if x > 8:
                x = 8
            ages[x] += 1
            
    return ages

    

if __name__ == "__main__":
    
    age = readages("ages.txt")

    clist = ['white','red', 'wheat','blue','green','brown','purple', 'orange', 'grey']
    
    ticklist = ["{0:2d}-{1:2d}".format(x,x+9) for x in range(0, 90, 10)]

    plt.style.use("ggplot")
    fig, ax = plt.subplots(1, 2, figsize=(16,9), dpi=100)

#create bar plot
    
    ax[0].bar(range(0, 90, 10), age, width=4.0, color=clist,
            edgecolor="black", tick_label=ticklist) 

    ax[0].hlines(0, 0, 80, colors='black')
    ax[0].set_title("Age Groups of Coffee Drinkers")
    ax[0].set_xlabel("Age Group")
    ax[0].set_ylabel("Number of People")

    
#create pie plot

    labs = ['Teens', '20s', '30s', '40s', '50s', '60s', '70 and up']
    
    ax[1].pie(age[1:-1], colors=clist[1:], labels=labs, wedgeprops={"edgecolor":"black"})

    ax[1].set_title("Age Groups of Coffee Drinkers")

    plt.savefig("/mnt/d/CS/saved_plots/barpie.png")
