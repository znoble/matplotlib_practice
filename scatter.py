#make a scatter plot of car data
#x axis - weight 
#y axis - mpg

import sys
import matplotlib.pyplot as plt

def readcars(fname):
    mpg = []
    wgt = []
    with open(fname, "r") as fd:
        for line in fd:
            a = line.rstrip().split(",")
            mpg.append(float(a[1]))
            wgt.append(float(a[2])/1000)
            
    return wgt, mpg

if __name__ == "__main__":

    x, y = readcars("cars.csv")
    
    plt.figure(figsize=(16,9), dpi=80)
    plt.style.use("fivethirtyeight")

    plt.scatter(x, y, marker='o', s=100, color='red', linewidth=1, edgecolor='black')

    plt.title("2020 Cars: Miles per Gallon vs. Weight")
    plt.xlabel('Weight in 1000s of Pounds')
    plt.ylabel('Miles per Gallon')

    plt.tight_layout()
    plt.savefig("/mnt/d/CS/saved_plots/scatter.png")
