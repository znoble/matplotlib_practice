
import sys
import matplotlib.pyplot as plt


def read_pop_file(fname):

    d = {}
    with open(fname) as fd:
        for line in fd:
            li = line.rstrip().split(':')
            d[li[0]] = list(map(lambda x: float(x)/1000000.0, li[1:]))

    return d

if __name__ == "__main__":

    pops = read_pop_file('pop.txt')     # y values for each state

    years = list(range(1900, 2020, 10)) # x values -- years of US Census

    plt.style.use("ggplot")
    plt.figure(figsize=(12,9), dpi=100)

    for state in ['Indiana', 'Ohio', 'Illinois', 'Michigan', 'California', 'Texas', 'Kentucky']:
        plt.plot(years, pops[state], marker='o',linestyle='-', label=state)

    plt.title("State Population Growth")
    plt.xlabel("Years")
    plt.ylabel("Population in Millions")
    plt.xticks(years)
    plt.legend()
    plt.savefig("/mnt/d/CS/saved_plots/pop.png")
    
            
