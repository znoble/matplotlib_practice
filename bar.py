#bar plot in python

import sys
import os
import matplotlib.pyplot as plt


def readages(fname):

    ages = [0] * 9

    with open(fname, "r") as fd:
        for line in fd:
            x = int(line) // 10
            if x > 8:
                x = 8
            ages[x] += 1
            
    return ages

    

if __name__ == "__main__":
    
    age = readages("ages.txt")

    clist = ['white','red', 'wheat','blue','green','brown','purple', 'orange', 'grey']
    
    ticklist = ["{0:2d}-{1:2d}".format(x,x+9) for x in range(0, 90, 10)]
    
    plt.figure(figsize=(16,9), dpi=100)
    plt.style.use("fivethirtyeight")
    
    plt.bar(range(0, 90, 10), age, width=4.0, color=clist,
            edgecolor="black", tick_label=ticklist) 

    plt.hlines(0, 0, 80, colors='black')
    plt.title("Age Groups of Coffee Drinkers")
    plt.xlabel("Age Group")
    plt.ylabel("Number of People")

    plt.savefig("/mnt/d/CS/saved_plots/bar.png")
