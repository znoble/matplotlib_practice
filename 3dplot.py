import sys
from random import randint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

if __name__ == "__main__":

    x = [randint(0, 99) for _ in range(100)]
    y = [randint(0, 99) for _ in range(100)]
    z = [randint(0, 99) for _ in range(100)]

    fig = plt.figure(figsize=(12,9), dpi=100)
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(x,y,z,marker='o')

    plt.show()
    
